#include "RF.h"
#include "fifo.h"

/* RF SPI */
SPI_HandleTypeDef hspiRF;
const uint32_t spi_timeout_RF = 5000;

FIFO fifo_rf_tx;
FIFO fifo_rf_rx;

/* Address */
uint8_t address[5] = {0xE7,0xE7,0xE7,0xE7,0xE7};

extern FIFO fifo_comm_rf_tx;
extern FIFO fifo_comm_rf_rx;

enum state
{
	Receiver,
	Transmitter
};

enum state current_state = Receiver;

/* Register init values */
//LSB byte to MSB byte, MSB bit in each byte first BANK0 all registers
static uint8_t rf_bank0_init[][2] = {
/*0*/	{CONFIG_REG,		0xB0}, //PRX, PWR ON, CRC 1byte,
		{EN_AA_REG,			0x01}, // ACK enable on pipe0
		{EN_RXADDR_REG,		0x01}, //enable only data pipe 0
		{SETUP_AW_REG,		0x03}, // 5bytes address
		{SETUP_RETR_REG,	0x33}, // ARD: wait 1000us, ARC: try 3 times on fail
		{RF_CH_REG,			0x17}, // channel 17 2400+17 = 2417MHz
		{RF_SETUP_REG,		0x2F}, //2Mbps, high gain
/*7*/	{STATUS_REG,		0x0E},
		{OBSERVE_TX_REG,	0x00},
		{CD_REG,			0x00},
		{RX_ADDR_P2_REG,	0xC3}, // data pipe2 addres
		{RX_ADDR_P3_REG,	0xC4}, // data pipe3 addres
		{RX_ADDR_P4_REG,	0xC5}, // data pipe4 addres
		{RX_ADDR_P5_REG,	0xC6}, // data pipe5 addres
/*14*/	{RX_PW_P0_REG,		0x20}, // 32 byte RX payload
		{RX_PW_P1_REG,		0x20}, // 32 byte RX payload
		{RX_PW_P2_REG,		0x20}, // 32 byte RX payload
		{RX_PW_P3_REG,		0x20}, // 32 byte RX payload
		{RX_PW_P4_REG,		0x20}, // 32 byte RX payload
		{RX_PW_P5_REG,		0x20}, // 32 byte RX payload
/*20*/	{FIFO_STATUS_REG,	0x00},
		{DYNPD_REG,			0x3F}, // dynamic payload lenght for every pipe
/*22*/	{FEATURE_REG,		0x07}  // enable dynamic payload lenght enable payload with ack and enable tx no ack command
									};
//MSB byte to LSB byte, MSB bit in each byte first BANK1 0-8 registers
//LSB byte to MSB byte, MSB bit in each byte first BANK1 9-0E registers
static uint8_t rf_bank1_init[][5] = {
		{0x0, 0x40, 0x4B, 0x01, 0xE2},
		{0x1, 0xC0, 0x4B, 0x00, 0x00},
		{0x2, 0xD0, 0xFC, 0x8C, 0x02},
		{0x3, 0x99, 0x00, 0x39, 0x21},
		{0x4, 0xF9, 0x96, 0x82, 0xDB},
		{0x5, 0x24, 0x06, 0x0F, 0xB6},
		{0x6, 0x00, 0x00, 0x00, 0x00},
		{0x7, 0x00, 0x00, 0x00, 0x00},
		{0x8, 0x00, 0x00, 0x00, 0x00},
		{0x9, 0x00, 0x00, 0x00, 0x00},
		{0xA, 0x00, 0x00, 0x00, 0x00},
		{0xB, 0x00, 0x00, 0x00, 0x00},
		{0xC, 0x00, 0x12, 0x73, 0x00},
		{0xD, 0x36, 0xb4, 0x80, 0x00}
									};
static uint8_t rf_ramp[] = {0xE, 0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF};

/* RFM75 Commands */
static uint8_t rf_cmd_toggle_bank[] = {0x50, 0x53};
static uint8_t rf_cmd_activate[] = {0x50, 0x73};
static uint8_t rf_cmd_rx_pl_wid = 0x60;

/* Static Functions */
/* Initialization Functions */
static void rf_gpio_init();
static void rf_spi_init();
static void rf_reg_init();
static void rf_basic_init();
/* RFM75 Functions */
static void rf_select_bank(uint8_t bank);
static void rf_toggle_bank();
static void rf_activate();
static uint8_t rf_rx_pl_wid();
static void rf_set_rx0_address(uint8_t address[5]);
static void rf_set_tx_address(uint8_t address[5]);
static void Set_RF_CE(uint8_t);
static void RF_Write_Reg_Bit(uint8_t register_id, uint8_t register_pos, uint8_t pos_value);
static void RF_Write_Reg(uint8_t register_id, uint8_t data);
static void RF_Write_RegBuf(uint8_t register_id, uint8_t *data, uint8_t size);
static uint8_t RF_Read_Reg(uint8_t register_id);
static void RF_Write_Payload(uint8_t size);
static void RF_Read_Payload(uint8_t size);
static void RF_TX_Process();
static void RF_RX_Process();


void RF_Init(void)
{	
	HAL_Delay(1000);		// Power Up delay
	Fifo_Init(&fifo_rf_rx, 256);
	Fifo_Init(&fifo_rf_tx, 256);
	rf_gpio_init();
	rf_spi_init();
	rf_reg_init();
	rf_basic_init();
}

/* Initialization Functions */
static void rf_gpio_init()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*Configure GPIO pins : PAPin PAPin PAPin PAPin */
	GPIO_InitStruct.Pin = RF_CE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, RF_CE_Pin, GPIO_PIN_RESET);


	/*Configure GPIO pin : PtPin */
	GPIO_InitStruct.Pin = IRQ__Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IRQ__GPIO_Port, &GPIO_InitStruct);


	/*Configure GPIO pins : PAPin */
	GPIO_InitStruct.Pin = RF_SPI_NSS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(RF_SPI_NSS_Port, RF_SPI_NSS_Pin, GPIO_PIN_RESET);

	//HAL_NVIC_SetPriority(EXTI2_3_IRQn, 3, 0);
	//HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);					todo comment megszuntet
}

static void rf_spi_init()
{
    /* Peripheral clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();

    GPIO_InitTypeDef GPIO_InitStruct;
    /**SPI1 GPIO Configuration    
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PA7     ------> SPI1_MOSI 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF0_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(SPI1_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(SPI1_IRQn);

    /* RF configuration */
	hspiRF.Instance = SPI1;
	hspiRF.Init.Mode = SPI_MODE_MASTER;
	hspiRF.Init.Direction = SPI_DIRECTION_2LINES;
	hspiRF.Init.DataSize = SPI_DATASIZE_8BIT;
	hspiRF.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspiRF.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspiRF.Init.NSS = SPI_NSS_HARD_OUTPUT;
	hspiRF.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	hspiRF.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspiRF.Init.TIMode = SPI_TIMODE_DISABLE;
	hspiRF.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspiRF.Init.CRCPolynomial = 7;
	if (HAL_SPI_Init(&hspiRF) != HAL_OK)
		{
			Error_Handler(RF_Init_Error);
		}

}

static void rf_reg_init()
{
	rf_select_bank(0);

	RF_Write_Reg(rf_bank0_init[0][0],rf_bank0_init[0][1]);

	HAL_Delay(2);		//1.5ms power up delay

	for(uint8_t i = 1; i<21; i++)
	{
		RF_Write_Reg(rf_bank0_init[i][0],rf_bank0_init[i][1]);
	}

	rf_set_rx0_address(address);
	rf_set_tx_address(address);

	if(!RF_Read_Reg(FEATURE_REG))
	{
		rf_activate();
	}
	RF_Write_Reg(rf_bank0_init[22][0],rf_bank0_init[22][1]);
	RF_Write_Reg(rf_bank0_init[21][0],rf_bank0_init[21][1]);

	rf_select_bank(1);

	for(uint8_t i = 0; i<14; i++)
	{
		RF_Write_RegBuf(rf_bank1_init[i][0], &rf_bank1_init[i][1], 4);
	}

	RF_Write_RegBuf(rf_ramp[0], &rf_ramp[1], 11);

	HAL_Delay(100);

	rf_select_bank(0);
}

static void rf_select_bank(uint8_t bank)
{
	uint8_t curr_bank = RF_Read_Reg(STATUS_REG);

	if(bank)
	{
		if(!IsBit(curr_bank, 7))
		{
			rf_toggle_bank();
		}
	}
	else
	{
		if(IsBit(curr_bank, 7))
		{
			rf_toggle_bank();
		}
	}
}

static void rf_basic_init()
{
	RF_Write_Reg_Bit(CONFIG_REG , PWR_UP_BIT, SET);							//PWR_UP
	Set_RF_CE(SET);

}
/***/

/* RFM75 Functions */
static void rf_toggle_bank()
{
	SPI_RF_Enable(1);
	SPI_RF_Send(rf_cmd_toggle_bank,2);
	SPI_RF_Enable(0);
}

static void rf_activate()
{
	SPI_RF_Enable(1);
	SPI_RF_Send(rf_cmd_activate,2);
	SPI_RF_Enable(0);
}

static void rf_set_rx0_address(uint8_t address[5])
{
	uint8_t reg_id = RX_ADDR_P0_REG + WRITE_REG_COMMAND;

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Send(address,5);
	SPI_RF_Enable(0);
}


static void rf_set_tx_address(uint8_t address[5])
{
	uint8_t reg_id = TX_ADDR_REG + WRITE_REG_COMMAND;

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Send(address,5);
	SPI_RF_Enable(0);
}

/* length of the received payload */
static uint8_t rf_rx_pl_wid()
{
	uint8_t data;

	SPI_RF_Enable(1);
	SPI_RF_Send(&rf_cmd_rx_pl_wid,1);
	SPI_RF_Receive(&data, 1);
	SPI_RF_Enable(0);

	return data;
}

/***/

/* RF controller Functions */
static void Set_RF_CE(uint8_t enable)										//enable = 1 --> enable, enable = 0 --> disable
{
	if(enable == 0)															// disable
	{
		HAL_GPIO_WritePin(GPIOA, RF_CE_Pin, GPIO_PIN_RESET);
	}
	else																	// enable
	{
		HAL_GPIO_WritePin(GPIOA, RF_CE_Pin, GPIO_PIN_SET);
	}
}

static void RF_Write_Reg_Bit(uint8_t register_id, uint8_t register_pos, uint8_t pos_value)
{
	uint8_t reg_data;
	if((register_id > 0x1f) && (register_id != READ_RX_PAYLOAD_COMMAND) && (register_id != WRITE_TX_PAYLOAD_COMMAND))
	{
		Error_Handler(RF_Invalid_Reg_ID);
	}

	uint8_t curr_data = RF_Read_Reg(register_id);							//Reads back the registers current value
	uint8_t reg_id = register_id + WRITE_REG_COMMAND;						//Sets the W_REGISTER cmomand for RF SPI

	if(pos_value == 0)
	{
		reg_data = ClearBit(curr_data, register_pos);
	}
	else
	{
		reg_data = SetBit(curr_data, register_pos);
	}

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Send(&reg_data,1);
	SPI_RF_Enable(0);
}

static void RF_Write_Reg(uint8_t register_id, uint8_t data)
{
	uint8_t reg_data = data;
	if((register_id > 0x1f) && (register_id != READ_RX_PAYLOAD_COMMAND) && (register_id != WRITE_TX_PAYLOAD_COMMAND))
	{
		Error_Handler(RF_Invalid_Reg_ID);
	}

	uint8_t reg_id = register_id + WRITE_REG_COMMAND;						//Sets the W_REGISTER cmomand for RF SPI

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Send(&reg_data,1);
	SPI_RF_Enable(0);
}

static void RF_Write_RegBuf(uint8_t register_id, uint8_t *data, uint8_t size)
{
	if((register_id > 0x1f) && (register_id != READ_RX_PAYLOAD_COMMAND) && (register_id != WRITE_TX_PAYLOAD_COMMAND))
	{
		Error_Handler(RF_Invalid_Reg_ID);
	}

	uint8_t reg_id = register_id + WRITE_REG_COMMAND;						//Sets the W_REGISTER cmomand for RF SPI

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Send(data,size);
	SPI_RF_Enable(0);
}

static void RF_RX_Process()
{
	if (current_state == Receiver)
	{
		volatile uint8_t size = rf_rx_pl_wid();
		if (size != 0)
			RF_Read_Payload(size);
		while(0 == fifo_rf_rx.empty)
		{
			uint8_t temp;
			if (Fifo_Pop(&fifo_rf_rx, &temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
			if (Fifo_Push(&fifo_comm_rf_rx, temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
	}
}

static void RF_TX_Process()
{
	if (0 == fifo_comm_rf_tx.empty)
	{
		RF_Enter_TX();
	}
	while(0 == fifo_comm_rf_tx.empty)
	{
		uint32_t size = 0;
		if (fifo_comm_rf_tx.full)
		{
			size = fifo_comm_rf_tx.size;
		}
		else
		{
			size = ((fifo_comm_rf_tx.size - (fifo_comm_rf_tx.first-fifo_comm_rf_tx.last)) % fifo_comm_rf_tx.size);
		}
		if (size > 32)
		{
			size = 32;
		}
		for ( uint32_t i = 0; i<size; i++)
		{
			uint8_t temp;
			if (Fifo_Pop(&fifo_comm_rf_tx, &temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
			if (Fifo_Push(&fifo_rf_tx, temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}

		RF_Write_Payload(size);
	}
	if (current_state == Transmitter)
	{
		RF_Enter_RX();
	}
}

static uint8_t RF_Read_Reg(uint8_t register_id)
{
	uint8_t reg_data;
	uint8_t reg_id = register_id;

	if((register_id > 0x1f) && (register_id != READ_RX_PAYLOAD_COMMAND) && (register_id != WRITE_TX_PAYLOAD_COMMAND))
	{
		Error_Handler(RF_Invalid_Reg_ID);
	}

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Receive(&reg_data,1);
	SPI_RF_Enable(0);

	return reg_data;
}

static void RF_Write_Payload(uint8_t size)
{
	if(fifo_rf_tx.empty)
	{
		return;
	}

	uint8_t reg_id = WRITE_TX_PAYLOAD_COMMAND;
	uint8_t data;
	uint8_t loopcounter = 0;
	uint8_t fifo_status = RF_Read_Reg(FIFO_STATUS_REG);
	if ((fifo_status & (1<<TX_FULL)))
	{
		RF_Flush_TX();
		return;
	}

	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	for(loopcounter = 0; loopcounter < size; loopcounter++)
	{
		if (Fifo_Pop(&fifo_rf_tx, &data) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		SPI_RF_Send(&data,1);
	}
	SPI_RF_Enable(0);

	volatile uint8_t is_send_succesfull = RF_Read_Reg(STATUS_REG);
	if (is_send_succesfull & (1<<TX_DS_BIT))
	{
		RF_Write_Reg_Bit(STATUS_REG, TX_DS_BIT, SET);
	}
	RF_Write_Reg_Bit(STATUS_REG, MAX_RT, SET);
}

static void RF_Read_Payload(uint8_t size)
{
	if(fifo_rf_rx.full)
	{
		return;
	}

	uint8_t reg_id = READ_RX_PAYLOAD_COMMAND;
	uint8_t data;
	uint8_t loopcounter = 0;

	uint8_t fifo_status = RF_Read_Reg(FIFO_STATUS_REG);
	if ((fifo_status & (1<<RX_EMPTY)))
	{
		return;
	}

	uint8_t status = RF_Read_Reg(STATUS_REG);
	if (status & (1<<RX_DR_BIT))
	{
		SPI_RF_Enable(1);
		SPI_RF_Send(&reg_id,1);
		for(loopcounter = 0; loopcounter < size; loopcounter++)
		{
			SPI_RF_Receive(&data,1);
			if (Fifo_Push(&fifo_rf_rx, data) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
		SPI_RF_Enable(0);
		RF_Write_Reg_Bit(reg_id, RX_DR_BIT, SET);
	}
}
/***/

/* RF SPI controller Functions */
void SPI_RF_Enable(int enable)												//enable = 1 --> enable, enable = 0 --> disable
{
	if(enable == 0)															// disable
	{
		HAL_GPIO_WritePin(RF_SPI_NSS_Port, RF_SPI_NSS_Pin, GPIO_PIN_SET);
	}
	else																	// enable
	{
		HAL_GPIO_WritePin(RF_SPI_NSS_Port, RF_SPI_NSS_Pin, GPIO_PIN_RESET);
	}

}

Error_Code SPI_RF_Send(uint8_t* pData, uint16_t dataSize)
{
	if(HAL_SPI_Transmit(&hspiRF,pData,dataSize,spi_timeout_RF) != HAL_OK)
	{
		return RF_SPI_Error;
	}
	return OK;
}

Error_Code SPI_RF_Receive(uint8_t* pData, uint16_t dataSize)
{
	if(HAL_SPI_Receive(&hspiRF,pData,dataSize,spi_timeout_RF) != HAL_OK)
	{
		return RF_SPI_Error;
	}
	return OK;
}

Error_Code SPI_RF_SendReceive(uint8_t* pDataIn, uint8_t *pDataOut, uint16_t dataSize)
{
	if(HAL_SPI_TransmitReceive(&hspiRF,pDataOut,pDataIn,dataSize,spi_timeout_RF) != HAL_OK)
	{
		return RF_SPI_Error;
	}
	return OK;
}
/***/

/* RF Callback Functions */
void GPIO_EXTI_Callback_RF(uint16_t GPIO_Pin)
{
	uint8_t reg_id = STATUS_REG;
	uint8_t reg_data = RF_Read_Reg(reg_id);

	if(reg_data & (1<<RX_DR_BIT))
	{
		uint8_t size = rf_rx_pl_wid();
		if (size != 0)
		{
			if (current_state != Receiver)
			{
				RF_Enter_RX();
			}
			RF_Read_Payload(size);
		}
		RF_Write_Reg_Bit(reg_id, RX_DR_BIT, SET);
	}

	if(reg_data & (1<<TX_DS_BIT))
	{
		RF_Write_Reg_Bit(reg_id, TX_DS_BIT, SET);
	}
}
/***/

/* RF state control Functions */
void RF_Enter_RX()
{
	current_state = Receiver;
	RF_Flush_RX();
	Set_RF_CE(CLEAR);
	RF_Write_Reg_Bit(CONFIG_REG , 0, SET);		//PRIM_RX=01
	Set_RF_CE(SET);
}

void RF_Enter_TX()
{
	current_state = Transmitter;
	RF_Flush_TX();
	Set_RF_CE(CLEAR);
	RF_Write_Reg_Bit(CONFIG_REG , 0, RESET);	//PRIM_RX=0
	Set_RF_CE(SET);
}

void RF_Flush_TX()
{
	uint8_t reg_id = FLUSH_TX_COMMAND;
	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Enable(0);
}

void RF_Flush_RX()
{
	uint8_t reg_id = FLUSH_RX_COMMAND;
	SPI_RF_Enable(1);
	SPI_RF_Send(&reg_id,1);
	SPI_RF_Enable(0);
}

void RF_Process()
{
	RF_RX_Process();
	RF_TX_Process();
}
