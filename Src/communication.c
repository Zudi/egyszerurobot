#include "communication.h"
#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "kisrobot.pb.h"
#include "general.h"
#include "fifo.h"
#include "error.h"
#include "stm32l0xx_hal.h"

uint32_t waiting_for_ACK = 0;
uint32_t waiting_for_ACK_ID = 0;
uint32_t waiting_for_ACK_counter = 0;

FIFO fifo_comm_rf_tx;
FIFO fifo_comm_rf_rx;
FIFO fifo_comm_wifi_rx;
FIFO fifo_comm_wifi_tx;

extern CRC_HandleTypeDef hcrc;
extern uint32_t messageID;
extern uint32_t ID;
extern FIFO fifo_wrappedMessage_rx;

/* Static Functions */
/* Process Functions */
static void Communication_Process_Wifi();
static void Communication_Process_Rf();
static void Communication_Process_Wifi_Received_Data(uint8_t* COBS_buffer, uint8_t COBS_size, uint8_t received_CRC_value);
static void Communication_Process_Rf_Received_Data(uint8_t* COBS_buffer, uint8_t COBS_size, uint8_t received_CRC_value);

static void Communication_Process_Wifi()
{
	if (1 == waiting_for_ACK)
	{
		return;
	}
	while (0 == fifo_comm_wifi_rx.empty)
	{
		uint8_t COBS_buffer[WrappedMessage_size+2];
		uint8_t COBS_size = 0;
		if (Fifo_Pop(&fifo_comm_wifi_rx, &COBS_size) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		uint8_t received_CRC_value = 0;
		if (Fifo_Pop(&fifo_comm_wifi_rx, &received_CRC_value) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		uint32_t i = 0;
		uint8_t adat;
		do
		{
			if (Fifo_Pop(&fifo_comm_wifi_rx, &adat) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
			COBS_buffer[i] = adat;
			i++;
		}
		while ((i < COBS_size+1) && (adat != 0x00) && i < WrappedMessage_size+1);

		if ((i == COBS_size+1) && (adat == 0x00))
		{
			Communication_Process_Wifi_Received_Data(COBS_buffer,COBS_size, received_CRC_value);
		}
		else if((i == COBS_size+1) && (adat != 0x00))
		{
			Communication_Process_Wifi_Received_Data(COBS_buffer,COBS_size, received_CRC_value);

			while (adat != 0x00 && 0 == fifo_comm_wifi_rx.empty)
			{
				uint8_t throw_away;
				if (Fifo_Pop(&fifo_comm_wifi_rx, &throw_away) != OK)
				{
					Error_Handler_Fifo(Buffer_Empty);
				}
			}
		}
		else if((i != COBS_size+1) && (adat == 0x00))
		{
			Communication_Process_Wifi_Received_Data(COBS_buffer,(i-1), received_CRC_value);
		}
	}
}


static void Communication_Process_Rf()
{
	while(0 == fifo_comm_rf_rx.empty)
	{
		uint8_t COBS_buffer[WrappedMessage_size+2];
		uint8_t COBS_size;
		if (Fifo_Pop(&fifo_comm_rf_rx, &COBS_size) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		uint8_t received_CRC_value = 0;
		if (Fifo_Pop(&fifo_comm_rf_rx, &received_CRC_value) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		uint32_t i = 0;
		uint8_t adat;
		do
		{
			if (Fifo_Pop(&fifo_comm_rf_rx, &adat) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
			COBS_buffer[i] = adat;
			i++;
		}
		while ((i < COBS_size+1) && (adat != 0x00));

		if ((i == COBS_size+1) && (adat == 0x00))
		{
			Communication_Process_Rf_Received_Data(COBS_buffer,COBS_size,received_CRC_value);
		}
		else if((i == COBS_size+1) && (adat != 0x00))
		{
			Communication_Process_Rf_Received_Data(COBS_buffer,COBS_size,received_CRC_value);

			while (adat != 0x00 && 1 != fifo_comm_rf_rx.empty)
			{
				uint8_t throw_away;
				if (Fifo_Pop(&fifo_comm_rf_rx, &throw_away) != OK)
				{
					Error_Handler_Fifo(Buffer_Empty);
				}
			}
		}
		else if((i != COBS_size+1) && (adat == 0x00))
		{
			Communication_Process_Rf_Received_Data(COBS_buffer,(i-1),received_CRC_value);
		}
	}
}


static void Communication_Send_ACK(uint32_t ReceivedMessageID, FIFO* where_to_send)
{
	WrappedMessage message = WrappedMessage_init_zero;
	message.messageID = messageID;
	messageID++;
	message.robotID = 0;
	message.which_oneof_option = WrappedMessage_responseInfoMessage_tag;
	message.oneof_option.responseInfoMessage.parameter = ReceivedMessageID;
	message.oneof_option.responseInfoMessage.Status = _status_MESSAGE_RECEIVED;
	uint8_t buffer[WrappedMessage_size];
	uint8_t COBS_buffer[WrappedMessage_size+2];
	pb_ostream_t ostream = pb_ostream_from_buffer(buffer, WrappedMessage_size);
	pb_encode(&ostream, WrappedMessage_fields, &message);
	uint8_t CRC_value = 0;
	CRC_value = HAL_CRC_Calculate(&hcrc,(uint32_t *)buffer,ostream.bytes_written);
	uint8_t COBS_size = StuffData(buffer, ostream.bytes_written, COBS_buffer);
	COBS_buffer[COBS_size] = 0x00;
	Fifo_Push(where_to_send, COBS_size);
	Fifo_Push(where_to_send, CRC_value);
	for (int i=0; i<COBS_size+1; i++)
	{
		Fifo_Push(where_to_send, COBS_buffer[i]);
	}
}

static void Communication_Process_Wifi_Received_Data(uint8_t* COBS_buffer, uint8_t COBS_size, uint8_t received_CRC_value)
{
	uint32_t j = 0;
	uint8_t encoded_buffer[WrappedMessage_size];
	WrappedMessage wrappedMessage = WrappedMessage_init_zero;
	uint8_t decoded_COBS_size = UnStuffData(COBS_buffer,COBS_size,encoded_buffer);
	uint8_t calculated_CRC_value = 0;
	calculated_CRC_value = HAL_CRC_Calculate(&hcrc,(uint32_t *)encoded_buffer,decoded_COBS_size);
	if (calculated_CRC_value == received_CRC_value)
	{
		pb_istream_t istream = pb_istream_from_buffer(encoded_buffer, decoded_COBS_size);
		pb_decode(&istream, WrappedMessage_fields, &wrappedMessage);
		if (wrappedMessage.robotID == ID)
		{
			if (Fifo_Push_Multiple(&fifo_wrappedMessage_rx, (uint8_t *)&wrappedMessage, sizeof(WrappedMessage)) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
			Communication_Send_ACK(wrappedMessage.messageID, &fifo_comm_wifi_tx);
		}
		else
		{
			waiting_for_ACK = 1;
			waiting_for_ACK_ID = wrappedMessage.messageID;
			if (Fifo_Push(&fifo_comm_rf_tx, COBS_size) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
			if (Fifo_Push(&fifo_comm_rf_tx, received_CRC_value) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
			do
			{
				if (Fifo_Push(&fifo_comm_rf_tx, COBS_buffer[j]) != OK)
				{
					Error_Handler_Fifo(Buffer_Full);
				}
				j++;
			}
			while (j < COBS_size+1);
		}
	}
}

static void Communication_Process_Rf_Received_Data(uint8_t* COBS_buffer, uint8_t COBS_size, uint8_t received_CRC_value)
{
	uint32_t j = 0;
	uint8_t encoded_buffer[WrappedMessage_size];
	WrappedMessage wrappedMessage = WrappedMessage_init_zero;
	uint8_t decoded_COBS_size = UnStuffData(COBS_buffer,COBS_size,encoded_buffer);
	uint8_t calculated_CRC_value = 0;
	calculated_CRC_value = HAL_CRC_Calculate(&hcrc,(uint32_t *)encoded_buffer,decoded_COBS_size);
	if (calculated_CRC_value == received_CRC_value)
	{
		pb_istream_t istream = pb_istream_from_buffer(encoded_buffer, decoded_COBS_size);
		pb_decode(&istream, WrappedMessage_fields, &wrappedMessage);
		if (wrappedMessage.robotID == ID)
		{
			if (Fifo_Push_Multiple(&fifo_wrappedMessage_rx, (uint8_t *)&wrappedMessage, sizeof(WrappedMessage)) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
			Communication_Send_ACK(wrappedMessage.messageID, &fifo_comm_rf_tx);
		}
		if (ID == 1)	//master
		{
			if (1 == waiting_for_ACK)
			{
				if (waiting_for_ACK_ID == wrappedMessage.oneof_option.responseInfoMessage.parameter)
				{
					waiting_for_ACK = 0;
					waiting_for_ACK_counter = 0;
				}
			}
			if(0 == wrappedMessage.robotID)
			{
				if (Fifo_Push(&fifo_comm_wifi_tx, COBS_size) != OK)
				{
					Error_Handler_Fifo(Buffer_Full);
				}
				if (Fifo_Push(&fifo_comm_wifi_tx, received_CRC_value) != OK)
				{
					Error_Handler_Fifo(Buffer_Full);
				}
				do
				{
					if (Fifo_Push(&fifo_comm_wifi_tx, COBS_buffer[j]) != OK)
					{
						Error_Handler_Fifo(Buffer_Full);
					}
					j++;
				}
				while (j < COBS_size+1);
			}
		}
	}
}


/* Interface Functions */
void Communication_Init()
{
	Fifo_Init(&fifo_comm_rf_rx, 256);
	Fifo_Init(&fifo_comm_rf_tx, 256);
	Fifo_Init(&fifo_comm_wifi_rx, 256);
	Fifo_Init(&fifo_comm_wifi_tx, 256);
}

void Communication_Process()
{
	if (ID == 1)	//master
	{
		Communication_Process_Wifi();
	}
	Communication_Process_Rf();
}
/***/
