#include "LED.h"

/* Initialization Functions */
void LED_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin */
  GPIO_InitStruct.Pin = LED_red_Pin|LED_yellow_Pin|LED_blue_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_red_Pin|LED_yellow_Pin|LED_blue_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED_green_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_green_Pin, GPIO_PIN_RESET);
}
/***/

/* LED control functions */
void LED_Switch(uint8_t led, uint8_t state)
{
	switch(led)
	{
	case(LED_RED):
			if(state)
			{
				HAL_GPIO_WritePin(GPIOA, LED_red_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_red_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_BLUE):
			if(state)
			{
				HAL_GPIO_WritePin(GPIOA, LED_blue_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_blue_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_YELLOW):
			if(state)
			{
				HAL_GPIO_WritePin(GPIOA, LED_yellow_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_yellow_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_GREEN):
			if(state)
			{
				HAL_GPIO_WritePin(GPIOB, LED_green_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOB, LED_green_Pin, GPIO_PIN_RESET);
			}
			break;
	default:
			break;
	}
}

void LED_Toggle(uint8_t led)
{
	switch(led)
	{
	case(LED_RED):
			if(HAL_GPIO_ReadPin(GPIOA,LED_red_Pin) == 0)
			{
				HAL_GPIO_WritePin(GPIOA, LED_red_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_red_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_BLUE):
			if(HAL_GPIO_ReadPin(GPIOA,LED_blue_Pin) == 0)
			{
				HAL_GPIO_WritePin(GPIOA, LED_blue_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_blue_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_YELLOW):
			if(HAL_GPIO_ReadPin(GPIOA,LED_yellow_Pin) == 0)
			{
				HAL_GPIO_WritePin(GPIOA, LED_yellow_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LED_yellow_Pin, GPIO_PIN_RESET);
			}
			break;
	case(LED_GREEN):
			if(HAL_GPIO_ReadPin(GPIOA,LED_green_Pin) == 0)
			{
				HAL_GPIO_WritePin(GPIOB, LED_green_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOB, LED_green_Pin, GPIO_PIN_RESET);
			}
			break;
	default:
			break;
	}
}
/***/
