#include "stm32l0xx_hal.h"
#include "stm32l0xx.h"
#include "servo.h"

TIM_HandleTypeDef htim21;
ADC_HandleTypeDef hadc;
extern enum direction direction_in_progress;


void ADC_Init(void)
{
	ADC_ChannelConfTypeDef sConfig;
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_ADC1_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	hadc.Instance = ADC1;
	hadc.Init.OversamplingMode = DISABLE;
	hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
	hadc.Init.Resolution = ADC_RESOLUTION_12B;
	hadc.Init.SamplingTime = ADC_SAMPLETIME_19CYCLES_5;
	hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ContinuousConvMode = ENABLE;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc.Init.DMAContinuousRequests = DISABLE;
	hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc.Init.LowPowerAutoWait = DISABLE;
	hadc.Init.LowPowerFrequencyMode = DISABLE;
	hadc.Init.LowPowerAutoPowerOff = DISABLE;
	if (HAL_ADC_Init(&hadc) != HAL_OK)
	{
		Error_Handler(ADC_Init_Error);
	}

	if (HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED) != HAL_OK)
	{
	    Error_Handler(ADC_Init_Error);
	}

	sConfig.Channel = ADC_CHANNEL_9;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
	    Error_Handler(ADC_Init_Error);
	}

	if (HAL_ADC_Start(&hadc) != HAL_OK)
	{
	    Error_Handler(ADC_Init_Error);
	}
}

void ADC_Timer_init()
{
	/* Peripheral clock enable */
	__HAL_RCC_TIM21_CLK_ENABLE();
	/* Peripheral interrupt init */
	HAL_NVIC_SetPriority(TIM21_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM21_IRQn);
	/* TIM21 Configuration */
	htim21.Instance = TIM21;
	htim21.Init.Prescaler = 24000;
	htim21.Init.CounterMode = TIM_COUNTERMODE_DOWN;
	htim21.Init.Period = 50;
	htim21.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	if (HAL_TIM_Base_Init(&htim21) != HAL_OK)
	{
		Error_Handler(IR_Init_Error);
	}
	TIM_MasterConfigTypeDef sMasterConfig;
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim21, &sMasterConfig) != HAL_OK)
	{
		Error_Handler(IR_Init_Error);
	}
	HAL_TIM_Base_Start_IT(&htim21);
}

void TIM21_IRQHandler(void)
{
 HAL_TIM_IRQHandler(&htim21);
}

void PeriodElapsedCallback_ADC(TIM_HandleTypeDef *htim)
{
    HAL_ADC_PollForConversion(&hadc, 10);

    if ((HAL_ADC_GetState(&hadc) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC)
    {
      uint32_t value = HAL_ADC_GetValue(&hadc);

      if ((value > 2000) && direction_in_progress == ACCELERATE)
      {
  		command_pointer_parameter_t parameter = {0};
  		Servo_Stop(parameter);
      }
    }
}
