#include "fifo.h"
#include <stdlib.h>

extern uint8_t wifi_rx_watchdog_request;
extern uint8_t wifi_rx_watchdog_counter;
extern FIFO fifo_wifi_rx;

void EnterCritical()
{
	__asm volatile("cpsid i");
	__asm volatile("dsb");
	__asm volatile("isb");
}

void ExitCritical()
{
	__asm volatile("cpsie i");
}

void Fifo_Init(FIFO* fifo, uint32_t size)
{
	fifo->empty = 1;
	fifo->full = 0;
	fifo->first = 0;
	fifo->last = 0;
	fifo->size = size;
	fifo->p_data = malloc(sizeof(uint8_t)*size);
}

Error_Code Fifo_Push(FIFO* fifo, uint8_t data)
{
	EnterCritical();
	if (fifo->full)
	{
		void ExitCritical();
		return Buffer_Full;
	}
	fifo->p_data[fifo->last] = data;

	if (fifo->last == fifo->size-1)
	{
		fifo->last = 0;
	}
	else
	{
		(fifo->last)++;
	}

	if (fifo->last == fifo->first)
	{
		fifo->full = 1;
	}

	if (fifo->empty)
	{
		fifo->empty = 0;
	}
	ExitCritical();
	return OK;
}

Error_Code Fifo_Pop(FIFO* fifo, uint8_t* data)
{
	EnterCritical();
	if(fifo->empty)
	{
		ExitCritical();
		return Buffer_Empty;
	}

	*data = fifo->p_data[fifo->first];
	if (fifo == &fifo_wifi_rx)
	{
		wifi_rx_watchdog_request = 0;
		wifi_rx_watchdog_counter = 0;
	}

	if(fifo->first == fifo->size-1)
	{
		fifo->first = 0;
	}
	else
	{
		(fifo->first)++;
	}

	if (fifo->first == fifo->last)
	{
		fifo->empty = 1;
	}

	if (fifo->full)
	{
		fifo->full = 0;
	}
	ExitCritical();
	return OK;
}


Error_Code Fifo_Push_Multiple(FIFO* fifo, uint8_t *data, uint32_t size)
{
	if (size == 0)
	{
		return OK;
	}
	uint32_t free_place = (fifo->size - (fifo->last - fifo->first)) % fifo->size;

	if ((fifo->full || (free_place < size)) && !fifo->empty)
	{
		return Buffer_Full;
	}
	for (uint32_t i = 0; i < size; i++)
	{
		EnterCritical();
		fifo->p_data[fifo->last] = *(data+i);
			if (fifo->last == fifo->size-1)
			{
				fifo->last = 0;
			}
			else
			{
				(fifo->last)++;
			}
		ExitCritical();
	}

	if (fifo->last == fifo->first)
	{
		fifo->full = 1;
	}

	if (fifo->empty)
	{
		fifo->empty = 0;
	}
	return OK;
}

Error_Code Fifo_Pop_Multiple(FIFO* fifo, uint8_t* data, uint32_t size)
{
	if (size == 0)
	{
		return OK;
	}
	if(fifo->empty)
	{
		return Buffer_Empty;
	}
	for (uint32_t i = 0; i < size; i++)
	{
		EnterCritical();
		*(data+i) = fifo->p_data[fifo->first];
		if(fifo->first == fifo->size-1)
		{
			fifo->first = 0;
		}
		else
		{
			(fifo->first)++;
		}
		ExitCritical();
	}


	if (fifo->first == fifo->last)
	{
		fifo->empty = 1;
	}

	if (fifo->full)
	{
		fifo->full = 0;
	}
	return OK;
}
