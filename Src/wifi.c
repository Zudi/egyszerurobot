#include "wifi.h"
#include <string.h>
#include "fifo.h"

#define AT_NUMBER_OF_REPLIES 9

FIFO fifo_wifi_tx;
FIFO fifo_wifi_rx;

/* Error Registers */
extern uint8_t error_init_reg;
extern uint8_t error_wifi_reg;
extern FIFO fifo_comm_wifi_tx;
extern FIFO fifo_comm_wifi_rx;
/***/

/* Wifi UART */
UART_HandleTypeDef huartWifi;
/***/

/* Wifi variables */
uint8_t wifi_connected = 0;
uint8_t wifi_unfinished_rx_data = 0;
uint8_t wifi_tx_active = 0;
uint8_t wifi_send_request = 0;
uint8_t wifi_send_ready = 0;
uint8_t wifi_rx_state = 0;
uint8_t wifi_tx_watchdog_request = 0;
uint8_t wifi_tx_watchdog_counter = 0;
uint8_t wifi_rx_watchdog_request = 0;
uint8_t wifi_rx_watchdog_counter = 0;
uint16_t wifi_send_size = 0;

/* AT commands */
char at_command_at[]			= "AT\r\n";
char at_command_uart_def[]		= "AT+UART_DEF=115200,8,1,0,0\r\n";
char at_command_uart_cur[]		= "AT+UART_CUR=115200,8,1,0,0\r\n";
char at_command_cwmode[]		= "AT+CWMODE_CUR=3\r\n";
char at_command_cipmux[]		= "AT+CIPMUX=1\r\n";
char at_command_cipserver[]		= "AT+CIPSERVER=1\r\n";
char at_command_cipsend[] 		= "AT+CIPSEND=0,0\r\n";
char at_command_ate[]			= "ATE0\r\n";
//char at_command_cipclose[] = "AT+CIPCLOSE=0\r\n";			//15*/

/* AT command replies */
char *at_command_reply_vector[AT_NUMBER_OF_REPLIES] = {	"OK\r\n",
														"SEND OK\r\n",
														"CONNECT",
														"ERROR",
														"busy s...",
														"busy p...",
														"AT\r\r\n\r\nOK\r\n",
														"ATE0\r\r\n\r\nOK\r\n",
														"FAIL\r\n\r\n"};



/* Static Functions */
/* Initialization Functions */
static void wifi_usart_init(uint32_t baudrate);
static void wifi_esp_uart_init();
static void wifi_base_init();
/* Process Functions */
static void IncrementScout(uint16_t *scout);
static void DecrementScout(uint16_t *scout);
static void Wifi_rx_process();
static void Wifi_tx_process();
static void Wifi_setup_send(uint16_t size);
static void Wifi_CRLF_send();
static uint8_t Wifi_vector_tester(uint8_t state);


void Wifi_Init()
{
	Fifo_Init(&fifo_wifi_rx, 256);
	Fifo_Init(&fifo_wifi_tx, 256);
	wifi_usart_init((uint32_t)76800);
	HAL_Delay(WIFI_UART_BOOTUP_RESPONSE);
	wifi_esp_uart_init();

	if( !IsBit(error_init_reg, WIFI_INIT_ERROR_FLAG))
	{
		wifi_base_init();
	}
}


static void wifi_usart_init(uint32_t baudrate)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* USART1 clock enable */
	__HAL_RCC_USART1_CLK_ENABLE();

	/**USART1 GPIO Configuration
	PB6     ------> USART1_TX
	PB7     ------> USART1_RX
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF0_USART1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(USART1_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

	huartWifi.Instance = USART1;
	huartWifi.Init.BaudRate = baudrate;
	huartWifi.Init.WordLength = UART_WORDLENGTH_8B;
	huartWifi.Init.StopBits = UART_STOPBITS_1;
	huartWifi.Init.Parity = UART_PARITY_NONE;
	huartWifi.Init.Mode = UART_MODE_TX_RX;
	huartWifi.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huartWifi.Init.OverSampling = UART_OVERSAMPLING_16;
	huartWifi.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huartWifi.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_SWAP_INIT;
	huartWifi.AdvancedInit.Swap = UART_ADVFEATURE_SWAP_ENABLE;
	if (HAL_UART_Init(&huartWifi) != HAL_OK)
	{
		Error_Handler(Wifi_Init_Error);
	}

    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
	__HAL_UART_ENABLE_IT(&huartWifi, UART_IT_ERR);

    /* Enable the Data Register not empty Interrupts */
	__HAL_UART_ENABLE_IT(&huartWifi, UART_IT_RXNE);
}

static void wifi_esp_uart_init()
{
	char response[12];
	for(int i = 0; i<strlen(at_command_at); i++)
	{
		if (Fifo_Push(&fifo_wifi_tx, at_command_at[i]) != OK)
		{
			Error_Handler_Fifo(Buffer_Full);
		}
	}
	fifo_wifi_rx.last = fifo_wifi_rx.first;
	fifo_wifi_rx.empty = 1;
	UART_Wifi_Send();
	HAL_Delay(WIFI_UART_RESPONSE);

	for(int i = 0; i<11; i++)
	{
		if (Fifo_Pop(&fifo_wifi_rx, (uint8_t*)&response[i]) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
	}
	response[11] = '\0';

	if(0 == strcmp(response, "AT\r\r\n\r\nOK\r\n"))
	{
		return;
	}
	else
	{
		wifi_usart_init((uint32_t)115200);
	}
}

static void wifi_base_init()
{
	error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_FRAME_ERROR_FLAG);
	error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_NOISE_ERROR_FLAG);
	error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_OVERRUN_ERROR_FLAG);
	fifo_wifi_rx.last = fifo_wifi_rx.first;
	fifo_wifi_rx.empty = 1;

	for(int i = 0; i<strlen(at_command_at); i++)
	{
		if (Fifo_Push(&fifo_wifi_tx, at_command_at[i]) != OK)
		{
			Error_Handler_Fifo(Buffer_Full);
		}
	}

	UART_Wifi_Send();
	HAL_Delay(WIFI_UART_RESPONSE);

	if(IsBit(error_wifi_reg, WIFI_UART_FRAME_ERROR_FLAG) || IsBit(error_wifi_reg, WIFI_UART_NOISE_ERROR_FLAG) || IsBit(error_wifi_reg, WIFI_UART_OVERRUN_ERROR_FLAG))
	{
		error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_FRAME_ERROR_FLAG);
		error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_NOISE_ERROR_FLAG);
		error_wifi_reg = ClearBit(error_wifi_reg, WIFI_UART_OVERRUN_ERROR_FLAG);

		Error_Handler(Wifi_Init_Error);
	}
	else
	{
		for(int i = 0; i<strlen(at_command_ate); i++)
		{
			if (Fifo_Push(&fifo_wifi_tx, at_command_ate[i]) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
		UART_Wifi_Send();
		HAL_Delay(WIFI_UART_RESPONSE);

		for(int i = 0; i<strlen(at_command_cwmode); i++)
		{
			if (Fifo_Push(&fifo_wifi_tx, at_command_cwmode[i]) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
		UART_Wifi_Send();
		HAL_Delay(WIFI_UART_RESPONSE);


		for(int i = 0; i<strlen(at_command_cipmux); i++)
		{
			if (Fifo_Push(&fifo_wifi_tx, at_command_cipmux[i]) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
		UART_Wifi_Send();
		HAL_Delay(WIFI_UART_RESPONSE);


		for(int i = 0; i<strlen(at_command_cipserver); i++)
		{
			if (Fifo_Push(&fifo_wifi_tx, at_command_cipserver[i]) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
		UART_Wifi_Send();
		HAL_Delay(WIFI_UART_RESPONSE);
	}
}
/***/

/* Wifi UART controller Functions */
Error_Code UART_Wifi_Send()
{
	wifi_tx_active = 1;
	uint8_t temp;
	if (Fifo_Pop(&fifo_wifi_tx, &temp) != OK)
	{
		Error_Handler_Fifo(Buffer_Empty);
	}
	huartWifi.Instance->TDR = temp;

	__HAL_UART_ENABLE_IT(&huartWifi, UART_IT_TXE);

	return OK;
}
/***/

/* Process Functions */
static void IncrementScout(uint16_t *scout)
{
	if(*scout == fifo_wifi_rx.size-1)
	{
		*scout = 0;
	}
	else
	{
		*scout += 1;
	}
}

static void DecrementScout(uint16_t *scout)
{
	if(*scout == 0)
	{
		*scout = fifo_wifi_rx.size-1;
	}
	else
	{
		*scout -= 1;
	}
}

static void Wifi_rx_process()
{
	uint16_t scout = fifo_wifi_rx.first;
	char scout_message[ERROR_MESSAGE_LENGTH];
	uint16_t scout_message_counter;

	switch(wifi_rx_state)
	{
	case(0):
		while(0 == wifi_rx_state && (fifo_wifi_rx.empty != 1 && scout != fifo_wifi_rx.last))
		{
			switch(fifo_wifi_rx.p_data[scout])
			{
			case('>'):
				wifi_rx_state = '>';
				break;
			case('O'):
				wifi_rx_state = 'O';
				break;
			case('S'):
				wifi_rx_state = 'S';
				break;
			case('A'):
				wifi_rx_state = 'A';
				break;
			case('C'):
				wifi_rx_state = 'C';
				break;
			case('E'):
				wifi_rx_state = 'E';
				break;
			case('b'):
				wifi_rx_state = 'b';
				break;
			case('+'):
				wifi_rx_state = '+';
				break;
			case('R'):
				wifi_rx_state = 'R';
				break;
			default:
				IncrementScout(&scout);
				break;
			}
		}

		//fifo filled with invalid characters
		while(scout != fifo_wifi_rx.first)
		{
			uint8_t throw_away;
			if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
		}

		break;
	case('>'):
		if(wifi_send_request)
		{
				wifi_send_ready = 1;
		}

		uint8_t throw_away;
		if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		wifi_rx_state = 0;
		break;
	case('O'):
		wifi_rx_state = Wifi_vector_tester('O');
		break;
	case('S'):
		wifi_rx_state = Wifi_vector_tester('S');
		break;
	case('A'):
		wifi_rx_state = Wifi_vector_tester('A');
		break;
	case('C'):
		for(scout_message_counter = 0; scout_message_counter < 7; scout_message_counter++, IncrementScout(&scout))
		{
			scout_message[scout_message_counter] = fifo_wifi_rx.p_data[scout];
		}
		scout_message[scout_message_counter] = '\0';

		if(0 == strcmp(scout_message, "CONNECT"))
		{
				for(scout_message_counter = 0; scout_message_counter < 7; scout_message_counter++)
				{
					uint8_t throw_away;
					if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
					{
						Error_Handler_Fifo(Buffer_Empty);
					}
				}
				wifi_rx_state = 0;
				wifi_connected = 1;
		}

		if(0 == strcmp(scout_message, "CLOSED"))
		{
				for(scout_message_counter = 0; scout_message_counter < 7; scout_message_counter++)
				{
					uint8_t throw_away;
					if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
					{
						Error_Handler_Fifo(Buffer_Empty);
					}
				}
				wifi_rx_state = 0;
				wifi_connected = 0;
		}
		break;
	case('E'):
		wifi_rx_state = Wifi_vector_tester('E');
		break;
	case('b'):
		wifi_rx_state = Wifi_vector_tester('b');
		break;
	case('+'):
		for(scout_message_counter = 0; scout_message_counter < 7; scout_message_counter++, IncrementScout(&scout))
		{
			scout_message[scout_message_counter] = fifo_wifi_rx.p_data[scout];
		}
		scout_message[scout_message_counter] = '\0';

		if(0 == strcmp(scout_message, "+IPD,0,"))
		{
			uint16_t rec_data_num = 0;
			while(fifo_wifi_rx.p_data[scout] != ':')
			{
				if(fifo_wifi_rx.p_data[scout] > 47 && fifo_wifi_rx.p_data[scout] < 58)
				{
					rec_data_num *= 10;
					rec_data_num += fifo_wifi_rx.p_data[scout] - 48;

					IncrementScout(&scout);
				}
				else
				{
					rec_data_num = 0;
					break;
				}
			}
			if(0 == rec_data_num)
			{
				break;
			}
			IncrementScout(&scout);

			for(uint8_t i = 0; i < rec_data_num; i++, IncrementScout(&scout))
			{
				if(scout == fifo_wifi_rx.last)
				{
					rec_data_num = 0;
				}
			}

			if(0 == rec_data_num)
			{
				break;
			}

			while(fifo_wifi_rx.p_data[fifo_wifi_rx.first] != ':')
			{
				uint8_t throw_away;
				if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
				{
					Error_Handler_Fifo(Buffer_Empty);
				}
			}
			uint8_t throw_away;
			if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}

			uint8_t data;
			while((fifo_wifi_rx.first != scout))
			{
				if (Fifo_Pop(&fifo_wifi_rx, &data) != OK)
				{
					Error_Handler_Fifo(Buffer_Empty);
				}
				if (Fifo_Push(&fifo_comm_wifi_rx, data) != OK)
				{
					Error_Handler_Fifo(Buffer_Full);
				}
			}

			wifi_rx_state = 0;
		}
		break;
	case('R'):
		for(scout_message_counter = 0; scout_message_counter < 5; scout_message_counter++, IncrementScout(&scout))
		{
			scout_message[scout_message_counter] = fifo_wifi_rx.p_data[scout];
		}
		scout_message[scout_message_counter] = '\0';

		if(0 == strcmp(scout_message, "Recv "))
		{
			uint16_t rec_data_num = 0;
			while(fifo_wifi_rx.p_data[scout] != ' ')
			{
				if(fifo_wifi_rx.p_data[scout] > 47 && fifo_wifi_rx.p_data[scout] < 58)
				{
					rec_data_num *= 10;
					rec_data_num += fifo_wifi_rx.p_data[scout] - 48;

					scout_message[scout_message_counter] = fifo_wifi_rx.p_data[scout];
					scout_message_counter++;
					IncrementScout(&scout);
				}
				else
				{
					rec_data_num = 0;
					break;
				}
			}
			if(0 == rec_data_num)
			{
				break;
			}

			for(uint8_t i = 0; i < 6; i++, IncrementScout(&scout))
			{
				if(scout == fifo_wifi_rx.last)
				{
					rec_data_num = 0;
				}
			}
			if(0 == rec_data_num)
			{
				break;
			}

			while(fifo_wifi_rx.first != scout)
			{
				uint8_t throw_away;
				if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
				{
					Error_Handler_Fifo(Buffer_Empty);
				}
			}

			wifi_rx_state = 0;
		}
		break;
	}
}

static void Wifi_tx_process()
{
	if(wifi_send_request == 0)
	{
		if(fifo_comm_wifi_tx.full)
		{
			wifi_send_request = 1;
			Wifi_setup_send(fifo_comm_wifi_tx.size);
			wifi_send_size = fifo_comm_wifi_tx.size;

			Wifi_CRLF_send();


			wifi_tx_watchdog_request = 1;
		}
		if(!fifo_comm_wifi_tx.empty)
		{
			wifi_send_request = 1;
			wifi_send_size = ((fifo_comm_wifi_tx.size - (fifo_comm_wifi_tx.first - fifo_comm_wifi_tx.last)) % fifo_comm_wifi_tx.size);
			Wifi_setup_send(wifi_send_size);

			Wifi_CRLF_send();

			wifi_tx_watchdog_request = 1;
		}
	}

	if(wifi_send_ready)
	{
		for(uint8_t i = 0; i<wifi_send_size; i++)
		{
			uint8_t temp;
			if (Fifo_Pop(&fifo_comm_wifi_tx, &temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Empty);
			}
			if (Fifo_Push(&fifo_wifi_tx, temp) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}

		wifi_send_ready = 0;
		wifi_send_request = 0;
		wifi_tx_watchdog_request = 0;
		wifi_tx_watchdog_counter = 0;
	}
}

static void Wifi_setup_send(uint16_t size)
{
	uint16_t i;
	uint8_t j;
	uint8_t data;

	for(i = 0; i< 13; i++)
	{
		if (Fifo_Push(&fifo_wifi_tx, at_command_cipsend[i]) != OK)
		{
			Error_Handler_Fifo(Buffer_Full);
		}
	}

	for(i = WIFI_SEND_MAX_DIVISOR, j = 0; i!=0; i /=10)
	{
		data = size / i;
		data -= j;

		j += data;
		j *= 10;

		if(j!=0)
		{
			data += 48;
			if (Fifo_Push(&fifo_wifi_tx, data) != OK)
			{
				Error_Handler_Fifo(Buffer_Full);
			}
		}
	}
}

static void Wifi_CRLF_send()
{
	if (Fifo_Push(&fifo_wifi_tx, '\r') != OK)
	{
		Error_Handler_Fifo(Buffer_Full);
	}
	if (Fifo_Push(&fifo_wifi_tx, '\n') != OK)
	{
		Error_Handler_Fifo(Buffer_Full);
	}
}

static uint8_t Wifi_vector_tester(uint8_t state)
{
	uint16_t scout = fifo_wifi_rx.first;
	char scout_message[ERROR_MESSAGE_LENGTH];
	uint16_t reply_vector_counter;
	for(reply_vector_counter = 0; reply_vector_counter < AT_NUMBER_OF_REPLIES; reply_vector_counter++)
	{
		if(at_command_reply_vector[reply_vector_counter][0] == state)
		{
			uint16_t scout_message_counter;
			for(scout_message_counter = 0, scout = fifo_wifi_rx.first; scout_message_counter < strlen(at_command_reply_vector[reply_vector_counter]); scout_message_counter++, IncrementScout(&scout))
			{
				scout_message[scout_message_counter] = fifo_wifi_rx.p_data[scout];
			}
			scout_message[scout_message_counter] = '\0';

			if(0 == strcmp(scout_message, at_command_reply_vector[reply_vector_counter]))
			{
				for(scout_message_counter = 0; scout_message_counter < strlen(at_command_reply_vector[reply_vector_counter]); scout_message_counter++)
				{
					IncrementScout(&scout);
					uint8_t throw_away;
					if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
					{
						Error_Handler_Fifo(Buffer_Empty);
					}
				}
				return 0;
			}
		}
	}
	return state;
}
/***/

/* Wifi IRQ Handler Functions */
void USART1_IRQHandler(void)
{
	uint32_t isrflags   = READ_REG(huartWifi.Instance->ISR);
	uint32_t cr1its     = READ_REG(huartWifi.Instance->CR1);
	uint32_t cr3its		= READ_REG(huartWifi.Instance->CR3);

	//Receive
	if(((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
	{
		if (Fifo_Push(&fifo_wifi_rx, huartWifi.Instance->RDR) != OK)
		{
			Error_Handler_Fifo(Buffer_Full);
		}
	}

	//Transmit
	if(((isrflags & USART_ISR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
	{
		uint8_t temp;
		if (Fifo_Pop(&fifo_wifi_tx, &temp) != OK)
		{
				Error_Handler_Fifo(Buffer_Empty);
		}
		huartWifi.Instance->TDR = temp;
		if (fifo_wifi_tx.empty)
		{
			CLEAR_BIT(huartWifi.Instance->CR1, USART_CR1_TXEIE);
			wifi_tx_active = 0;
		}
	}

    /* UART frame error interrupt occurred --------------------------------------*/
    if(((isrflags & USART_ISR_FE) != RESET) && ((cr3its & USART_CR3_EIE) != RESET))
    {
		__HAL_UART_CLEAR_IT(&huartWifi, UART_CLEAR_FEF);
		Error_Handler(Wifi_UART_frame);

    }

    /* UART noise error interrupt occurred --------------------------------------*/
    if(((isrflags & USART_ISR_NE) != RESET) && ((cr3its & USART_CR3_EIE) != RESET))
    {
		__HAL_UART_CLEAR_IT(&huartWifi, UART_CLEAR_NEF);
		Error_Handler(Wifi_UART_noise);
    }

    /* UART Over-Run interrupt occurred -----------------------------------------*/
    if(((isrflags & USART_ISR_ORE) != RESET) && (((cr1its & USART_CR1_RXNEIE) != RESET) || ((cr3its & USART_CR3_EIE) != RESET)))
	{
		__HAL_UART_CLEAR_IT(&huartWifi, UART_CLEAR_OREF);
		Error_Handler(Wifi_UART_overrun);
	}
}
/***/


void Wifi_tx_Watchdog_Elapsed()
{
	wifi_send_request = 0;
	wifi_tx_watchdog_request = 0;
	wifi_tx_watchdog_counter = 0;
}

void Wifi_rx_Watchdog_Elapsed()
{
	uint8_t throw_away;
	if (Fifo_Pop(&fifo_wifi_rx, &throw_away) != OK)
	{
		Error_Handler_Fifo(Buffer_Empty);
	}
	wifi_rx_state = 0;

	wifi_rx_watchdog_request = 0;
	wifi_rx_watchdog_counter = 0;
}

void Wifi_Process()
{
	Wifi_rx_process();
	if(wifi_connected)
	{
		Wifi_tx_process();
	}
	if(0 == fifo_wifi_tx.empty && 0 == wifi_tx_active)
	{
		UART_Wifi_Send();
	}
	return;
}
/***/
