#include "error.h"
#include "led.h"
#include <string.h>

/* Error Message */
char error_message[1024] = {"Default message"};

/* Error Flags */
/* Error Register Init */
uint8_t error_init_reg = 0;
/* Error Register Servo */
uint8_t error_servo_reg = 0;
/* Error Register IR */
uint8_t error_ir_reg = 0;
/* Error Register RF */
uint8_t error_rf_reg = 0;
/* Error Register Wifi */
uint8_t error_wifi_reg = 0;


void Error_Handler_Fifo(Error_Code error)
{
	//There is not Error handler for the fifo errors, but it can be implemented here.
}

/* Error Handling Functions */
void Error_Handler(Error_Code error)
{
	switch(error)
	{
	case(OK):
		break;
	case(General_Timer_Init_Error):
		error_init_reg = SetBit(error_init_reg, STATE_MACHINE_INIT_ERROR_FLAG);
		strcpy(error_message, "State Machine Init FAILED");
		break;
	case(SystemClock_Init_Error):
		strcpy(error_message, "SystemClock Init FAILED ");
		break;
	case(Servo_Init_Error):
		error_init_reg = SetBit(error_init_reg, SERVO_INIT_ERROR_FLAG);
		strcpy(error_message, "Servo Init FAILED");
		break;
	case(IR_Init_Error):
		error_init_reg = SetBit(error_init_reg, IR_INIT_ERROR_FLAG);
		strcpy(error_message, "IR Init FAILED");
		break;
	case(RF_Init_Error):
		error_init_reg = SetBit(error_init_reg, RF_INIT_ERROR_FLAG);
		strcpy(error_message, "RF Init FAILED");
		break;
	case(RF_SPI_Error):
		strcpy(error_message, "RF SPI Communication FAILED");
		break;
	case(RF_Invalid_Reg_ID):
			strcpy(error_message, "Invalid Register ID RF");
		break;
	case(Wifi_Init_Error):
		error_init_reg = SetBit(error_init_reg, WIFI_INIT_ERROR_FLAG);
		strcpy(error_message, "Wifi Init FAILED");
		break;
	case(Wifi_UART_frame):
		error_wifi_reg = SetBit(error_wifi_reg, WIFI_UART_FRAME_ERROR_FLAG);
		break;
	case(Wifi_UART_noise):
		error_wifi_reg = SetBit(error_wifi_reg, WIFI_UART_NOISE_ERROR_FLAG);
		break;
	case(Wifi_UART_overrun):
		error_wifi_reg = SetBit(error_wifi_reg, WIFI_UART_OVERRUN_ERROR_FLAG);
		break;
	case(Command_Pointer_Buffer_Full):
			strcpy(error_message, "Command_Pointer_Buffer_Full");
		break;
	case(Command_Pointer_Buffer_Empty):
			strcpy(error_message, "Command_Pointer_Buffer_Empty");
		break;
	case(CRC_Init_Error):
			strcpy(error_message, "CRC_Init_Error");
		break;
	case(Buffer_Full):
			strcpy(error_message, "Buffer_Full");
		break;
	case(Buffer_Empty):
			strcpy(error_message, "Buffer_Empty");
		break;
	case(ADC_Init_Error):
			strcpy(error_message, "ADC init error");
		break;
	case(Invalid_Bit):
				strcpy(error_message, "Invalid Bit");
		break;
	default:
		break;
	}

	if ((error != Wifi_UART_frame) && (error != Wifi_UART_noise) && (error != Wifi_UART_overrun))
	{
		Set_Error_Indicator(error);
	}
}

void Set_Error_Indicator(Error_Code error)
{
	while(1)
	{
		volatile Error_Code error_code = error;
		LED_Toggle(LED_RED);
		HAL_Delay(1000);
	}
}
/***/
