#include "general.h"
#include "wifi.h"
#include "fifo.h"

#define StartBlock()	(code_ptr = dst++, code = 1)
#define FinishBlock()	(*code_ptr = code)

#define WIFI_WATCHDOG_COUNTS 100
#define WAITING_FOR_ACK_COUNTS 14

CRC_HandleTypeDef hcrc;
TIM_HandleTypeDef htim3;

/* General Timer as Watchdog */
extern uint8_t wifi_tx_watchdog_request;
extern uint8_t wifi_tx_watchdog_counter;
extern uint8_t wifi_rx_watchdog_request;
extern uint8_t wifi_rx_watchdog_counter;
extern uint32_t waiting_for_ACK;
extern uint32_t waiting_for_ACK_counter;
extern FIFO fifo_wifi_rx;

volatile uint32_t ID = 0;
volatile uint32_t messageID = 0;

void Dip_Switch_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DIP1_Pin|DIP2_Pin|DIP3_Pin|DIP4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DIP_GPIO_Port, &GPIO_InitStruct);
}

void Dip_Switch_Configure()
{
	ID = 0;
	GPIO_PinState state;
	state = HAL_GPIO_ReadPin(DIP_GPIO_Port,DIP1_Pin);
	if (state == GPIO_PIN_SET)
	{
		ID |= (1 << 0);
	}
	state = HAL_GPIO_ReadPin(DIP_GPIO_Port,DIP2_Pin);
	if (state == GPIO_PIN_SET)
	{
		ID |= (1 << 1);
	}
	state = HAL_GPIO_ReadPin(DIP_GPIO_Port,DIP3_Pin);
	if (state == GPIO_PIN_SET)
	{
		ID |= (1 << 2);
	}
	state = HAL_GPIO_ReadPin(DIP_GPIO_Port,DIP4_Pin);
	if (state == GPIO_PIN_SET)
	{
		ID |= (1 << 3);
	}
	ID += 1;
	messageID = 100 * ID;
}



/* General Timer Init */
void General_Timer_Init()
{
	TIM_MasterConfigTypeDef sMasterConfig;

	/* Peripheral clock enable */
	__HAL_RCC_TIM3_CLK_ENABLE();
	/* Peripheral interrupt init */
	HAL_NVIC_SetPriority(TIM3_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM3_IRQn);

	/* TIM3 Configuration */
	htim3.Instance = TIM3;
	htim3.Init.Prescaler = (SystemCoreClock / 1000) - 1;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = TIM3_PERIOD;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
	{
		Error_Handler(General_Timer_Init_Error);
	}

	__HAL_TIM_ENABLE_IT(&htim3, TIM_IT_UPDATE);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
	{
		Error_Handler(General_Timer_Init_Error);
	}

	if(HAL_TIM_Base_Start(&htim3) != HAL_OK)
	{
	      Error_Handler(General_Timer_Init_Error);
	}
}
/***/

void TIM3_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&htim3);
}

/* General Timer Callback Functions */
void PeriodElapsedCallback_General_Timer(TIM_HandleTypeDef *htim)
{
	if(wifi_tx_watchdog_request)
	{
		if(wifi_tx_watchdog_counter >= WIFI_WATCHDOG_COUNTS)
		{
			Wifi_tx_Watchdog_Elapsed();
		}
		else
		{
			wifi_tx_watchdog_counter ++;
		}
	}

	if (fifo_wifi_rx.empty == 0 && wifi_rx_watchdog_request == 0)
	{
		wifi_rx_watchdog_request = 1;
	}

	if(wifi_rx_watchdog_request)
	{
		if(wifi_rx_watchdog_counter >= WIFI_WATCHDOG_COUNTS)
		{
			Wifi_rx_Watchdog_Elapsed();
		}
		else
		{
			wifi_rx_watchdog_counter ++;
		}
	}

	if (waiting_for_ACK == 1)
	{
		if (waiting_for_ACK_counter >= WAITING_FOR_ACK_COUNTS)
		{
			waiting_for_ACK = 0;
			waiting_for_ACK_counter = 0;
		}
		else
		{
			waiting_for_ACK_counter++;
		}
	}
}
/***/

/* Generally Used Functions */
uint8_t SetBit(uint8_t data, uint8_t pos)
{
	if(pos > 7)
	{
		return data;														//Doesnt change data if pos is invalid
	}

	uint8_t pos_val = 1;
	for(int i = 0; i < pos; i++)
	{
		pos_val = pos_val << 1;
	}

	data |= pos_val;
	return data;
}

uint8_t ClearBit(uint8_t data, uint8_t pos)
{
	if(pos > 7)
	{
		return data;														//Doesnt change data if pos is invalid
	}

	uint8_t pos_val = 1;
	for(int i = 0; i < pos; i++)
	{
		pos_val = pos_val << 1;
	}

	data &= ~pos_val;
	return data;
}

uint8_t IsBit(uint8_t data, uint8_t pos)
{
	if(pos > 7)
	{
		Error_Handler(Invalid_Bit);
	}

	uint8_t pos_val = 1;
	for(int i = 0; i < pos; i++)
	{
		pos_val = pos_val << 1;
	}

	return data & pos_val;
}

size_t StuffData(const uint8_t *ptr, size_t length, uint8_t *dst)
{
	const uint8_t *start = dst, *end = ptr + length;
	uint8_t code, *code_ptr; /* Where to insert the leading count */

	StartBlock();
	while (ptr < end) {
		if (code != 0xFF) {
			uint8_t c = *ptr++;
			if (c != 0) {
				*dst++ = c;
				code++;
				continue;
			}
		}
		FinishBlock();
		StartBlock();
	}
	FinishBlock();
	return dst - start;
}

size_t UnStuffData(const uint8_t *ptr, size_t length, uint8_t *dst)
{
	const uint8_t *start = dst, *end = ptr + length;
	uint8_t code = 0xFF, copy = 0;

	for (; ptr < end; copy--) {
		if (copy != 0) {
			*dst++ = *ptr++;
		} else {
			if (code != 0xFF)
				*dst++ = 0;
			copy = code = *ptr++;
			if (code == 0)
				break; /* Source length too long */
		}
	}
	return dst - start;
}

void CRC_Init(void)
{
  __HAL_RCC_CRC_CLK_ENABLE();
  hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_DISABLE;
  hcrc.Init.GeneratingPolynomial = 0x07;
  hcrc.Init.CRCLength = CRC_POLYLENGTH_8B;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_DISABLE;
  hcrc.Init.InitValue = 0x00;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler(CRC_Init_Error);
  }
}

//void Protobuf_Speed_Test()
//{
//	uint32_t offset = 0;
//	volatile uint32_t PB_encoding_time = 0;
//	volatile uint32_t CRC_calculation_time = 0;
//	volatile uint32_t COBS_encoding_time = 0;
//	volatile uint32_t PB_decoding_time = 0;
//	volatile uint32_t COBS_decoding_time = 0;
//	uint8_t CRC_value = 0;
//	uint8_t buffer[WrappedMessage_size];
//	uint8_t COBS_buffer[WrappedMessage_size+2];
//	uint8_t decoded_COBS[WrappedMessage_size];
//	WrappedMessage wrappedMessage = WrappedMessage_init_zero;
//
//	HAL_TIM_Base_Start(&htim3);
//	HAL_TIM_Base_Stop(&htim3);
//	offset = __HAL_TIM_GET_COUNTER(&htim3);
//	__HAL_TIM_SET_COUNTER(&htim3,0);
//
//	WrappedMessage message = WrappedMessage_init_zero;
//	message.messageID = 1;
//	message.robotID = 1;
//	message.which_oneof_option = WrappedMessage_commandMessage_tag;
//	message.oneof_option.commandMessage.Command = Command__command_REVERSE;
//	message.oneof_option.commandMessage.parameter = 1500;
//	pb_ostream_t ostream = pb_ostream_from_buffer(buffer, WrappedMessage_size);
//	HAL_TIM_Base_Start(&htim3);
//	bool status = pb_encode(&ostream, WrappedMessage_fields, &message);
//	HAL_TIM_Base_Stop(&htim3);
//	PB_encoding_time = __HAL_TIM_GET_COUNTER(&htim3) - offset;
//	__HAL_TIM_SET_COUNTER(&htim3,0);
//	HAL_TIM_Base_Start(&htim3);
//	CRC_value = HAL_CRC_Calculate(&hcrc,(uint32_t *)buffer,ostream.bytes_written);
//	HAL_TIM_Base_Stop(&htim3);
//	CRC_calculation_time = __HAL_TIM_GET_COUNTER(&htim3) - offset;
//	__HAL_TIM_SET_COUNTER(&htim3,0);
//	HAL_TIM_Base_Start(&htim3);
//	uint8_t COBS_size = StuffData(buffer, ostream.bytes_written, COBS_buffer);
//	HAL_TIM_Base_Stop(&htim3);
//	COBS_encoding_time = __HAL_TIM_GET_COUNTER(&htim3) - offset;
//
//
//	__HAL_TIM_SET_COUNTER(&htim3,0);
//	HAL_TIM_Base_Start(&htim3);
//	uint8_t decoded_COBS_size = UnStuffData(COBS_buffer,COBS_size,decoded_COBS);
//	HAL_TIM_Base_Stop(&htim3);
//	COBS_decoding_time = __HAL_TIM_GET_COUNTER(&htim3) - offset;
//	__HAL_TIM_SET_COUNTER(&htim3,0);
//	pb_istream_t istream = pb_istream_from_buffer(decoded_COBS,decoded_COBS_size);
//	HAL_TIM_Base_Start(&htim3);
//	pb_decode(&istream, WrappedMessage_fields, &wrappedMessage);
//	HAL_TIM_Base_Stop(&htim3);
//	Fifo_Push_Multiple(&fifo_wrappedMessage_rx, &wrappedMessage, sizeof(WrappedMessage));
//	PB_decoding_time = __HAL_TIM_GET_COUNTER(&htim3) - offset;
//}
