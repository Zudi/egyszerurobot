#include "servo.h"

/* Source: https://cdn-shop.adafruit.com/product-files/2442/FS90R-V2.0_specs.pdf */
#define  PERIOD_VALUE       (2000 - 1)  /* Period Value: 32 MHz Clock source -> prescaled to 20 ms at 100 KHz  */
#define  PWM_STOP_VALUE     150        /* PWM STOP value: 		1,5 ms */
#define  PWM_CW_VALUE       175        /* PWM ClockWise value: 	> 1,5 ms */
#define  PWM_CCW_VALUE      125        /* PWM Counter-CW value: 	< 1,5 ms */

#define PWM_CW_MAX_VALUE	70
#define PWM_CCW_MAX_VALUE	230

enum direction direction_in_progress = STOP;

/*Directions:	- forward	: R=CW, L=CCW
 * 				- back		: R=CCW, L=CW
 * 				- right turn: R=CCW, L=CCW
 * 				- left turn	: R=CW, L=CW
*/

/* Timer handler declaration */
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim7;

/* Static Functions */
/* Initialization Functions */
static void servo_gpio_init();
static void servo_timer_init();
static void servo_timer_init_start();


void servo_Init(void)
{	
	servo_gpio_init();
	servo_timer_init();
	servo_timer_init_start();
}

/* Initialization Functions */
static void servo_gpio_init()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*Configure GPIO pins : PCPin PCPin */
	GPIO_InitStruct.Pin = servo_right_Pin|servo_left_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(servo_left_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(servo_left_GPIO_Port, servo_right_Pin|servo_left_Pin, GPIO_PIN_SET);
}

static void servo_timer_init()
{
	/* Peripheral clock enable */
	__HAL_RCC_TIM2_CLK_ENABLE();
	/* Peripheral interrupt init */
	HAL_NVIC_SetPriority(TIM2_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);

	htim2.Instance = TIM2;
	htim2.Init.Prescaler     = (SystemCoreClock / 100000) - 1;	// Shiftelve -1-el -> 100000 KHz-re �ll�t�s
	htim2.Init.Period        = PERIOD_VALUE;					//200-1
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.CounterMode   = TIM_COUNTERMODE_UP;
	if(HAL_TIM_PWM_Init(&htim2) != HAL_OK)
	{
	      Error_Handler(Servo_Init_Error);
	}

	__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);			//enable update interrupt

	TIM_MasterConfigTypeDef sMasterConfig;
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
	{
	     Error_Handler(Servo_Init_Error);
	}

	/*Configure the PWM channels*/
	/*Common configuration for all channels*/
	TIM_OC_InitTypeDef sConfigOC;
	sConfigOC.OCMode     = TIM_OCMODE_PWM1;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

	/* Set the pulse value for channel 1 */
	sConfigOC.Pulse = PWM_STOP_VALUE;
	if(HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
	     Error_Handler(Servo_Init_Error);
	}

	/* Set the pulse value for channel 2 */
	sConfigOC.Pulse = PWM_STOP_VALUE;
	if(HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
	{
	     Error_Handler(Servo_Init_Error);
	}



    __HAL_RCC_TIM7_CLK_ENABLE();
    HAL_NVIC_SetPriority(TIM7_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(TIM7_IRQn);

    htim7.Instance = TIM7;
    htim7.Init.Prescaler = 24000;
    htim7.Init.CounterMode = TIM_COUNTERMODE_DOWN;
    htim7.Init.Period = 1450;
    if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
    {
	    Error_Handler(Servo_Init_Error);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
    {
	    Error_Handler(Servo_Init_Error);
    }
}

static void servo_timer_init_start()
{
	/*Start PWM signals generation*/
	/* Start channel 1 */
	if(HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_1) != HAL_OK)
	{
	      Error_Handler(Servo_Init_Error);
	}

	/* Start channel 2 */
	if(HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_2) != HAL_OK)
	{
	      Error_Handler(Servo_Init_Error);
	}
}
/***/

/* Servo control Functions */
void Servo_Stop(command_pointer_parameter_t parameter)
{
	direction_in_progress = STOP;
	htim2.Instance->CCR1 = PWM_STOP_VALUE;
	htim2.Instance->CCR2 = PWM_STOP_VALUE;
}

void Servo_Accelerate(command_pointer_parameter_t parameter)
{
	uint32_t period = 43*parameter.parameter1;
	direction_in_progress = ACCELERATE;
	uint32_t speed = MOVING_SPEED;
	__HAL_TIM_SET_AUTORELOAD(&htim7, period);
	if(speed > 80) speed = 80;
	if(speed < 0)	speed = 0;
	HAL_TIM_Base_Start_IT(&htim7);
	htim2.Instance->CCR1 = 150 - speed;
	htim2.Instance->CCR2 = speed + 150;
	is_command_in_progress = 1;
}

void Servo_Reverse(command_pointer_parameter_t parameter)
{
	uint32_t period = 43*parameter.parameter1;
	direction_in_progress = REVERSE;
	uint32_t speed = MOVING_SPEED;
	__HAL_TIM_SET_AUTORELOAD(&htim7, period);
	if(speed > 80) speed = 80;
	if(speed < 0)	speed = 0;
	HAL_TIM_Base_Start_IT(&htim7);
	htim2.Instance->CCR1 = speed + 150;
	htim2.Instance->CCR2 = 150 - speed;
	is_command_in_progress = 1;
}


void Servo_Turn_Right(command_pointer_parameter_t parameter)
{
	uint32_t period = 4.5*parameter.parameter1;
	direction_in_progress = RIGHT;
	uint32_t speed = TURNING_SPEED;
	__HAL_TIM_SET_AUTORELOAD(&htim7, period);
	HAL_TIM_Base_Start_IT(&htim7);
	htim2.Instance->CCR1 = speed + 150;
	htim2.Instance->CCR2 = speed + 150;
	is_command_in_progress = 1;
}

void Servo_Turn_Left(command_pointer_parameter_t parameter)
{
	uint32_t period = 4.5*parameter.parameter1;
	direction_in_progress = LEFT;
	uint32_t speed = TURNING_SPEED;
	__HAL_TIM_SET_AUTORELOAD(&htim7, period);
	HAL_TIM_Base_Start_IT(&htim7);
	htim2.Instance->CCR1 = 150 - speed;
	htim2.Instance->CCR2 = 150 - speed;
	is_command_in_progress = 1;
}
/***/

/* IRQ Handler Functions */
void TIM2_IRQHandler(void)
{
 HAL_TIM_IRQHandler(&htim2);
}

void TIM7_IRQHandler(void)
{
 HAL_TIM_IRQHandler(&htim7);
}

/***/

/* Callback Functions */
void PWM_PulseFinishedCallback_Servo(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM2)
	{
		if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)								// servo_right PWM pulse change
		{
			HAL_GPIO_WritePin(servo_right_GPIO_Port, servo_right_Pin, GPIO_PIN_RESET);
		}
		if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)								// servo_left PWM pulse change
		{
			HAL_GPIO_WritePin(servo_left_GPIO_Port, servo_left_Pin, GPIO_PIN_RESET);
		}
	}
}

void PeriodElapsedCallback_Servo(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM2)
	{
			HAL_GPIO_WritePin(servo_left_GPIO_Port, servo_right_Pin|servo_left_Pin, GPIO_PIN_SET);	//PWM starting edge
	}

	if(htim->Instance == TIM7)
	{
		if (is_command_in_progress)
		{
			command_pointer_parameter_t parameter = {0};
			Servo_Stop(parameter);
			HAL_TIM_Base_Stop_IT(&htim7);
			is_command_in_progress = 0;
		}
	}
}
/***/
