#include "controller.h"
#include "kisrobot.pb.h"
#include "servo.h"
#include "fifo.h"
#include "general.h"
#include "stm32l0xx_hal.h"

uint32_t is_command_in_progress = 0;

FIFO fifo_wrappedMessage_rx;
FIFO fifo_wrappedMessage_tx;
FIFO fifo_command_pointer;
FIFO fifo_command_pointer_parameter;

extern uint32_t ID;

/* Static Functions */
static void Controller_rx_Process();
static void Controller_tx_Process();

static void Controller_rx_Process()
{
	if (0 == fifo_wrappedMessage_rx.empty)
	{
		WrappedMessage wrappedMessage = WrappedMessage_init_zero;
		command_pointer_parameter_t parameter;
		if (Fifo_Pop_Multiple(&fifo_wrappedMessage_rx, (uint8_t *)&wrappedMessage, sizeof(WrappedMessage)) != OK)
		{
			Error_Handler_Fifo(Buffer_Empty);
		}
		if(wrappedMessage.robotID == ID)
		{
			if(wrappedMessage.which_oneof_option == WrappedMessage_commandMessage_tag)
			{
				parameter.parameter1 = wrappedMessage.oneof_option.commandMessage.parameter;
				switch(wrappedMessage.oneof_option.commandMessage.Command)
				{
				void (*function)(command_pointer_parameter_t);
				case (Command__command_ACCELERATE):
						function = &Servo_Accelerate;
						Fifo_Push_Multiple(&fifo_command_pointer, (uint8_t *)(&function),sizeof(function));
						Fifo_Push_Multiple(&fifo_command_pointer_parameter,(uint8_t *)(&parameter),sizeof(command_pointer_parameter_t));
						break;
				case (Command__command_REVERSE):
						function = &Servo_Reverse;
						Fifo_Push_Multiple(&fifo_command_pointer, (uint8_t *)(&function),sizeof(function));
						Fifo_Push_Multiple(&fifo_command_pointer_parameter,(uint8_t *)(&parameter),sizeof(command_pointer_parameter_t));
						break;
				case (Command__command_STOP):
						function = &Servo_Stop;
						Fifo_Push_Multiple(&fifo_command_pointer, (uint8_t *)(&function),sizeof(function));
						Fifo_Push_Multiple(&fifo_command_pointer_parameter,(uint8_t *)(&parameter),sizeof(command_pointer_parameter_t));
						break;
				case (Command__command_TURNLEFT):
						function = &Servo_Turn_Left;
						Fifo_Push_Multiple(&fifo_command_pointer, (uint8_t *)(&function),sizeof(function));
						Fifo_Push_Multiple(&fifo_command_pointer_parameter,(uint8_t *)(&parameter),sizeof(command_pointer_parameter_t));
						break;
				case (Command__command_TURNRIGHT):
						function = &Servo_Turn_Right;
						Fifo_Push_Multiple(&fifo_command_pointer, (uint8_t *)(&function),sizeof(function));
						Fifo_Push_Multiple(&fifo_command_pointer_parameter,(uint8_t *)(&parameter),sizeof(command_pointer_parameter_t));
						break;
				}
			}
			else if(wrappedMessage.which_oneof_option == WrappedMessage_requestInfoMessage_tag)
			{
				//In the current scenerio there is not any requestInfoMessage that reach the controller module.
			}
		}
		else
		{
			//RobotID checked in the communication module and if the code is working fine this line is never reached.
			//Fifo_wrappedMessage_rx contains only those wrappedMessages which robotID is the same as the current robot's ID.
		}
	}
}

static void Controller_tx_Process()
{
	//There is not any requestInfoMessage ot responseInfoMessage in this scenerio and the ACK messages (what is a responseInfoMessage) are send
	//from the Communication module, so the controller module do not send any message to any other module.
}

void Controller_Init()
{
	Fifo_Init(&fifo_wrappedMessage_rx,1024);
	Fifo_Init(&fifo_wrappedMessage_tx,1024);
	Fifo_Init(&fifo_command_pointer,512);
	Fifo_Init(&fifo_command_pointer_parameter, 512);
}

void Controller_Process()
{
	Controller_rx_Process();
	Controller_tx_Process();
	if (0 == is_command_in_progress)
	{
		if (0 == fifo_command_pointer.empty)
		{
			void (*function)(command_pointer_parameter_t);
			command_pointer_parameter_t parameter;
			if ((Fifo_Pop_Multiple(&fifo_command_pointer,(uint8_t *)(&function), sizeof(function))) == OK)
			{
				if ((Fifo_Pop_Multiple(&fifo_command_pointer_parameter, (uint8_t *)(&parameter), sizeof(command_pointer_parameter_t))) == OK)
				{
					function(parameter);
				} else {
					Error_Handler_Fifo(Buffer_Empty);
				}
			} else {
				Error_Handler_Fifo(Buffer_Empty);
			}
		}
	}
}
