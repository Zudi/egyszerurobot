/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

void Dip_Switch_Init();
void Dip_Switch_Configure();

/* Watchdog Period */
#define TIM3_PERIOD (20-1)
void General_Timer_Init();
void TIM3_IRQHandler(void);
void PeriodElapsedCallback_General_Timer(TIM_HandleTypeDef *htim);

/* Generally Used Functions */
uint8_t SetBit(uint8_t data, uint8_t pos);
uint8_t ClearBit(uint8_t data, uint8_t pos);
uint8_t IsBit(uint8_t data, uint8_t pos);

size_t StuffData(const uint8_t *ptr, size_t length, uint8_t *dst);
size_t UnStuffData(const uint8_t *ptr, size_t length, uint8_t *dst);
void CRC_Init(void);



/***/
