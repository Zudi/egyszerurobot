/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once


#define servo_right_Pin 		GPIO_PIN_15
#define servo_right_GPIO_Port 	GPIOA
#define servo_left_Pin 			GPIO_PIN_1
#define servo_left_GPIO_Port 	GPIOA


#define RF_SPI_NSS_Pin 			GPIO_PIN_4
#define RF_SPI_NSS_Port 		GPIOA
#define RF_CE_Pin 				GPIO_PIN_3
#define RF_CE_GPIO_Port 		GPIOA
#define IRQ__Pin 				GPIO_PIN_2
#define IRQ__GPIO_Port 			GPIOA

#define LED_red_Pin 			GPIO_PIN_0
#define LED_red_GPIO_Port 		GPIOA
#define LED_green_Pin 			GPIO_PIN_10
#define LED_green_GPIO_Port 	GPIOB
#define LED_yellow_Pin 			GPIO_PIN_8
#define LED_yellow_GPIO_Port 	GPIOA
#define LED_blue_Pin 			GPIO_PIN_9
#define LED_blue_GPIO_Port 		GPIOA

#define DIP1_Pin				GPIO_PIN_9
#define DIP2_Pin				GPIO_PIN_8
#define DIP3_Pin				GPIO_PIN_7
#define DIP4_Pin				GPIO_PIN_6
#define DIP_GPIO_Port			GPIOC

