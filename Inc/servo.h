/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "error.h"
#include "general.h"
#include "controller.h"

#define	SPEED 30
#define MOVING_SPEED	SPEED
#define TURNING_SPEED	SPEED

extern uint32_t is_command_in_progress;

enum direction{
	LEFT,
	RIGHT,
	ACCELERATE,
	REVERSE,
	STOP
};

/* Initialization Functions */
void servo_Init(void);				//Servo initialization
/***/

/* Callback Functions */
void PWM_PulseFinishedCallback_Servo(TIM_HandleTypeDef *htim);
void PeriodElapsedCallback_Servo(TIM_HandleTypeDef *htim);
/***/

/* Servo control Functions */
void Servo_Stop();
void Servo_Accelerate(command_pointer_parameter_t parameter);
void Servo_Reverse(command_pointer_parameter_t parameter);
void Servo_Turn_Left(command_pointer_parameter_t parameter);
void Servo_Turn_Right(command_pointer_parameter_t parameter);
/***/
