/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

#include "error.h"
#include <stdint.h>

typedef struct
{
	uint8_t *p_data;
	uint32_t first;
	uint32_t last;
	uint32_t empty;
	uint32_t full;
	uint32_t size;
} FIFO;

void EnterCritical();
void ExitCritical();

void Fifo_Init(FIFO* fifo, uint32_t size);
Error_Code Fifo_Pop(FIFO* fifo, uint8_t* data);
Error_Code Fifo_Push(FIFO* fifo, uint8_t data);
Error_Code Fifo_Push_Multiple(FIFO* fifo, uint8_t *data, uint32_t size);
uint8_t Fifo_Pop_Multiple(FIFO* fifo, uint8_t* data, uint32_t size);
