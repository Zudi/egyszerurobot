/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "error.h"
#include "general.h"

#define WIFI_UART_RESPONSE 50
#define WIFI_UART_BOOTUP_RESPONSE 1000
#define ERROR_MESSAGE_LENGTH 100
#define WIFI_SEND_MAX_DIVISOR 1000

/* Initialization Functions */
void Wifi_Init();
/***/

/* Wifi UART controller Functions */
Error_Code UART_Wifi_Send();
/***/

/* Interface Functions */
void Wifi_Process();

void Wifi_tx_Watchdog_Elapsed();
void Wifi_rx_Watchdog_Elapsed();
/***/
