/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

void ADC_Init(void);
void ADC_Timer_init();
void TIM21_IRQHandler(void);
void PeriodElapsedCallback_ADC(TIM_HandleTypeDef *htim);
