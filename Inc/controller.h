/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

#include "stm32l0xx_hal.h"

typedef struct
{
	uint32_t parameter1;
} command_pointer_parameter_t;

/* Interface Functions */
void Controller_Init();
void Controller_Process();
/***/
