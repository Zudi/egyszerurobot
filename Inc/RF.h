/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "error.h"
#include "general.h"

/* RFM75 Commands */
#define READ_REG_COMMAND 0
#define WRITE_REG_COMMAND 32
#define READ_RX_PAYLOAD_COMMAND	0x61
#define WRITE_TX_PAYLOAD_COMMAND 0xA0
#define FLUSH_TX_COMMAND 0xE1
#define FLUSH_RX_COMMAND 0xE2
#define REUSE_TX_PL_COMMAND 0xE3

#define SET 1
#define CLEAR 0

/* RFM75 Registers */
/* Bank 0 */
#define CONFIG_REG 0x0
	#define PRIM_RX_BIT 0
	#define PWR_UP_BIT 1
#define EN_AA_REG 0x1
#define EN_RXADDR_REG 0x2
#define SETUP_AW_REG 0x3
#define SETUP_RETR_REG 0x4
#define RF_CH_REG 0x5
#define RF_SETUP_REG 0x6
#define STATUS_REG 0x7
	#define TX_DS_BIT 5
	#define RX_DR_BIT 6
	#define MAX_RT 4
#define OBSERVE_TX_REG 0x8
#define CD_REG 0x9
#define RX_ADDR_P0_REG 0xA
#define RX_ADDR_P1_REG 0xB
#define RX_ADDR_P2_REG 0xC
#define RX_ADDR_P3_REG 0xD
#define RX_ADDR_P4_REG 0xE
#define RX_ADDR_P5_REG 0xF
#define TX_ADDR_REG 0x10
#define RX_PW_P0_REG 0x11
#define RX_PW_P1_REG 0x12
#define RX_PW_P2_REG 0x13
#define RX_PW_P3_REG 0x14
#define RX_PW_P4_REG 0x15
#define RX_PW_P5_REG 0x16
#define FIFO_STATUS_REG 0x17
	#define TX_FULL	5
	#define TX_EMPTY 4
	#define RX_FULL 1
	#define RX_EMPTY 0
#define DYNPD_REG 0x1C
	#define DPL_P1 1
#define FEATURE_REG 0x1D
	#define EN_DPL 2



/* Initialization Functions */
void RF_Init(void);
/***/

/* RF SPI controller Functions */
void SPI_RF_Enable(int enable);
Error_Code SPI_RF_Send(uint8_t* pData, uint16_t dataSize);
Error_Code SPI_RF_Receive(uint8_t* pData, uint16_t dataSize);
Error_Code SPI_RF_SendReceive(uint8_t* pDataIn, uint8_t *pDataOut, uint16_t dataSize);
/***/

/* RF state control Functions */
void RF_Enter_RX();
void RF_Enter_TX();
/***/

/* RF Callback Functions */
void GPIO_EXTI_Callback_RF(uint16_t GPIO_Pin);
/***/

void RF_Flush_TX();
void RF_Flush_RX();

/* Interface Functions */
void RF_Process();
/***/


