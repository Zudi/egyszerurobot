/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "general.h"

/* Error Register Init */
#define SERVO_INIT_ERROR_FLAG 1
#define IR_INIT_ERROR_FLAG 2
#define RF_INIT_ERROR_FLAG 3
#define WIFI_INIT_ERROR_FLAG 4
#define	STATE_MACHINE_INIT_ERROR_FLAG 5
#define WIFI_UART_FRAME_ERROR_FLAG 1
#define WIFI_UART_NOISE_ERROR_FLAG 2
#define WIFI_UART_OVERRUN_ERROR_FLAG 3


typedef enum
{	OK,
	General_Timer_Init_Error,
	SystemClock_Init_Error,
	Servo_Init_Error,
	IR_Init_Error,
	RF_Init_Error,
	RF_SPI_Error,
	RF_Invalid_Reg_ID,
	Wifi_Init_Error,
	Wifi_UART_frame,
	Wifi_UART_noise,
	Wifi_UART_overrun,
	Command_Pointer_Buffer_Full,
	Command_Pointer_Buffer_Empty,
	CRC_Init_Error,
	Buffer_Full,
	Buffer_Empty,
	ADC_Init_Error,
	Invalid_Bit
}
Error_Code;

/* Error Handling Functions */
void Error_Handler_Fifo(Error_Code error);
void Error_Handler(Error_Code error);
void Set_Error_Indicator(Error_Code error);
/***/
