/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "general.h"

#define LED_OFF 0
#define LED_ON 1
#define LED_RED 2
#define LED_BLUE 3
#define LED_YELLOW 4
#define LED_GREEN 5

/* Initialization Functions */
void LED_Init(void);
/***/

/* LED control functions */
void LED_Switch(uint8_t led, uint8_t state);
void LED_Toggle(uint8_t led);
/***/
